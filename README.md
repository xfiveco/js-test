## Pimp my rect ( ͡° ͜ʖ ͡°)
_a simple JS app for xfive.co_

This is a specification for Xfive test JavaScript project. Use any technology of your convenience. It’s perfectly OK to use vanilla JS or any front-end framework or library.

Here’s how it should look like:

* The app should consist of 3 parts/sections: **Editor**, **Output**, **Gallery**. Editor should live-update the Output. It’s up to you if you show Gallery on the same page or under e.g. localhost:xxxx/gallery. What’s crucial: gallery’s state persists after browser restart. To obtain this goal, you’re allowed to use any technology which you feel convenient with (WS + IndexedDB, Firebase, Node+MongoDB, LocalStorage, whatever).
* **Output**: simple rectangular <div>
* **Editor**: it should have a set of inputs to adjust Output’s:
  * background colour (colour picker)
  * size (input which you think is the most suitable here)
  * border radius (please use a range picker)
  * A save button
* **Gallery**: a list of saved Output divs with preserved styling. It should be possible to:
  * remove Outputs from the list
  * [optional but very welcome] some animations or sorting/filtering options
* It should work well in the newest browsers (mobile Chrome and Safari included!).
* All third party dependencies (if there will be any) should be installed using either NPM or Bower.
* Whole thing should be splitted into modules (preferably with CommonJS or ES6 syntax but AMD is also fine).
* The layout is up to you! Feel free to use tools such as http://getbootstrap.com/ or https://www.muicss.com/. In case you’d like to roll your own CSS you’re more than welcome to do so.

**Project deadline**: Take your time but try to deliver it in 2 weeks time :)
